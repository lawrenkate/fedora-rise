# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
# Remove Unused Repos
# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
rm --force /etc/yum.repos.d/fedora-updates.repo
rm --force /etc/yum.repos.d/fedora-modular.repo
rm --force /etc/yum.repos.d/fedora-cisco-openh264.repo
rm --force /etc/yum.repos.d/fedora-updates-modular.repo
rm --force /etc/yum.repos.d/fedora-updates-testing.repo
rm --force /etc/yum.repos.d/fedora-updates-testing-modular.repo

# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
# Install Core Packages
# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
dnf install --assumeyes --allowerasing make
dnf install --assumeyes --allowerasing flatpak
dnf install --assumeyes --allowerasing nautilus
dnf install --assumeyes --allowerasing gnome-shell
dnf install --assumeyes --allowerasing gnome-terminal
dnf install --assumeyes --allowerasing gnome-software
dnf install --assumeyes --allowerasing bash-completion

# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
# Set Graphical Desktop Environment
# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
systemctl set-default graphical.target