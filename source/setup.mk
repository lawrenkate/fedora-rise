default:
	@- sudo make flatpak &>> log.txt

# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
# Flatpak
# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
flatpak: flatpak-del flatpak-add flatpak-install flatpak-config

flatpak-del:
	@- flatpak remote-delete --force fedora
	@- flatpak remote-delete --force fedora-testing

flatpak-add:
	@- flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak-install:
	@- flatpak install --assumeyes flathub org.gnome.eog
	@- flatpak install --assumeyes flathub org.gnome.Boxes
	@- flatpak install --assumeyes flathub org.gnome.clocks
	@- flatpak install --assumeyes flathub org.gnome.Calendar
	@- flatpak install --assumeyes flathub org.gnome.Calculator
	@- flatpak install --assumeyes flathub org.gnome.TextEditor

	@- flatpak install --assumeyes flathub com.spotify.Client
	@- flatpak install --assumeyes flathub org.mozilla.firefox

	@- flatpak install --assumeyes flathub org.godotengine.Godot
	@- flatpak install --assumeyes flathub com.visualstudio.code-oss
	@- flatpak install --assumeyes flathub com.github.Eloston.UngoogledChromium

	@- flatpak install --assumeyes flathub org.freedesktop.Sdk.Extension.node16

flatpak-config:
	@- flatpak override --user com.visualstudio.code-oss --env FLATPAK_ENABLE_SDK_EXT=node16